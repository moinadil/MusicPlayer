
package com.moin.musicplayer.lastfm;

import android.content.Context;

import com.moin.musicplayer.Config;

import java.util.Locale;
import java.util.Map;
import java.util.WeakHashMap;


public class Artist extends MusicEntry {

    protected final static ItemFactory<Artist> FACTORY = new ArtistFactory();

    protected Artist(final String name, final String url) {
        super(name, url);
    }

   
    public final static Artist getInfo(final Context context, final String artistOrMbid,
            final String apiKey) {
        return getInfo(context, artistOrMbid, Locale.getDefault(), apiKey);
    }

   
    public final static Artist getInfo(final Context context, final String artistOrMbid,
            final Locale locale, final String apiKey) {
        final Map<String, String> mParams = new WeakHashMap<String, String>();
        mParams.put("artist", artistOrMbid);
        if (locale != null && locale.getLanguage().length() != 0) {
            mParams.put("lang", locale.getLanguage());
        }
        final Result mResult = Caller.getInstance(context).call("artist.getInfo", apiKey, mParams);
        return ResponseBuilder.buildItem(mResult, Artist.class);
    }

   
    public final static Artist getCorrection(final Context context, final String artist) {
        Result result = null;
        try {
            result = Caller.getInstance(context).call("artist.getCorrection",
                    Config.LASTFM_API_KEY, "artist", artist);
            if (!result.isSuccessful()) {
                return null;
            }
            final DomElement correctionElement = result.getContentElement().getChild("correction");
            if (correctionElement == null) {
                return new Artist(artist, null);
            }
            final DomElement artistElem = correctionElement.getChild("artist");
            return FACTORY.createItemFromElement(artistElem);
        } catch (final Exception ignored) {
            return null;
        }
    }

    
    public final static PaginatedResult<Image> getImages(final Context context,
            final String artistOrMbid) {
        return getImages(context, artistOrMbid, -1, -1, Config.LASTFM_API_KEY);
    }

    
    public final static PaginatedResult<Image> getImages(final Context context,
            final String artistOrMbid, final int page, final int limit, final String apiKey) {
        final Map<String, String> params = new WeakHashMap<String, String>();
        params.put("artist", artistOrMbid);
        MapUtilities.nullSafePut(params, "page", page);
        MapUtilities.nullSafePut(params, "limit", limit);
        Result result = null;
        try {
            result = Caller.getInstance(context).call("artist.getImages", apiKey, params);
        } catch (final Exception ignored) {
            return null;
        }
        return ResponseBuilder.buildPaginatedResult(result, Image.class);
    }

    private final static class ArtistFactory implements ItemFactory<Artist> {

        /**
         * {@inheritDoc}
         */
        @Override
        public Artist createItemFromElement(final DomElement element) {
            if (element == null) {
                return null;
            }
            final Artist artist = new Artist(null, null);
            MusicEntry.loadStandardInfo(artist, element);
            return artist;
        }
    }
}
