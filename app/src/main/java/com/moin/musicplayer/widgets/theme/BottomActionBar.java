

package com.moin.musicplayer.widgets.theme;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.moin.musicplayer.R;
import com.moin.musicplayer.utils.ThemeUtils;

@SuppressWarnings("deprecation")
public class BottomActionBar extends RelativeLayout {

    /**
     * Resource name used to theme the bottom action bar
     */
    private static final String BOTTOM_ACTION_BAR = "bottom_action_bar";

    /**
     * @param context The {@link Context} to use
     * @param attrs The attributes of the XML tag that is inflating the view.
     * @throws NameNotFoundException
     */
    public BottomActionBar(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        // Initialze the theme resources
        final ThemeUtils resources = new ThemeUtils(context);
        // Theme the bottom action bar
        setBackgroundDrawable(resources.getDrawable(BOTTOM_ACTION_BAR));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        // Theme the selector
        final LinearLayout bottomActionBar = (LinearLayout)findViewById(R.id.bottom_action_bar);
        bottomActionBar.setBackgroundDrawable(new HoloSelector(getContext()));
    }
}
