

package com.devspark.appmsg;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.moin.musicplayer.R;

public class AppMsg {
	
	
    public static final int LENGTH_SHORT = 3000;

    
    public static final int LENGTH_LONG = 5000;
	
	
	public static final Style STYLE_ALERT = new Style(LENGTH_LONG, R.color.alert);
	
	
	public static final Style STYLE_CONFIRM = new Style(LENGTH_SHORT, R.color.confirm);
	
	public static final Style STYLE_INFO = new Style(LENGTH_SHORT, R.color.info);
    
    private final Activity mContext;
    private int mDuration = LENGTH_SHORT;
    private View mView;
    private LayoutParams mLayoutParams;

	
	public AppMsg(Activity context) {
		mContext = context;
	}
    
	
    public static AppMsg makeText(Activity context, CharSequence text, Style style) {
    	AppMsg result = new AppMsg(context);

        LayoutInflater inflate = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(R.layout.app_msg, null);
        v.setBackgroundResource(style.background);
        
        TextView tv = (TextView) v.findViewById(android.R.id.message);
        tv.setText(text);
        
        result.mView = v;
        result.mDuration = style.duration;

        return result;
    }

	
	public static AppMsg makeText(Activity context, int resId, Style style)
			throws Resources.NotFoundException {
		return makeText(context, context.getResources().getText(resId), style);
	}
    
    
    public void show() {
    	MsgManager manager = MsgManager.getInstance();
        manager.add(this);
    }
    
    
    boolean isShowing() {
      return mView != null && mView.getParent() != null;
    }

    
    public void cancel() {
    	MsgManager.getInstance().clearMsg(this);
    }
    
    
	public static void cancelAll() {
		MsgManager.getInstance().clearAllMsg();
	}
    
    
    public Activity getActivity() {
      return mContext;
    }
    
    
    public void setView(View view) {
        mView = view;
    }

    
    public View getView() {
        return mView;
    }
    
   
    public void setDuration(int duration) {
        mDuration = duration;
    }

    
    public int getDuration() {
        return mDuration;
    }

    
    public void setText(int resId) {
        setText(mContext.getText(resId));
    }
    
    
    public void setText(CharSequence s) {
        if (mView == null) {
            throw new RuntimeException("This AppMsg was not created with AppMsg.makeText()");
        }
        TextView tv = (TextView) mView.findViewById(android.R.id.message);
        if (tv == null) {
            throw new RuntimeException("This AppMsg was not created with AppMsg.makeText()");
        }
        tv.setText(s);
    }

    
    public LayoutParams getLayoutParams() {
        if (mLayoutParams == null) {
            mLayoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        }
        return mLayoutParams;
    }

    
    public AppMsg setLayoutParams(LayoutParams layoutParams) {
        mLayoutParams = layoutParams;
        return this;
    }

    public AppMsg setLayoutGravity(int gravity) {
        mLayoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, gravity);
        return this;
    }

	
    public static class Style {
		
		private final int duration;
		private final int background;

	
		public Style(int duration, int resId) {
			this.duration = duration;
			this.background = resId;
		}

		
		public int getDuration() {
			return duration;
		}

		public int getBackground() {
			return background;
		}
		
		@Override
		public boolean equals(Object o) {
			if (!(o instanceof AppMsg.Style)) {
				return false;
			}
			Style style = (Style) o;
			return style.duration == duration 
					&& style.background == background;
		}
		
	}

}
