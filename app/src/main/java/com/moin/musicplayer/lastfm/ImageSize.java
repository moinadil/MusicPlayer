

package com.moin.musicplayer.lastfm;

public enum ImageSize {

    LARGE, LARGESQUARE, ORIGINAL, EXTRALARGE

}
