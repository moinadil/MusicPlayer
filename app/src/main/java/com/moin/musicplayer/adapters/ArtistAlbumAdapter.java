

package com.moin.musicplayer.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.moin.musicplayer.R;
import com.moin.musicplayer.Config;
import com.moin.musicplayer.cache.ImageFetcher;
import com.moin.musicplayer.model.Album;
import com.moin.musicplayer.ui.MusicHolder;
import com.moin.musicplayer.ui.fragments.profile.ArtistAlbumFragment;
import com.moin.musicplayer.utils.ApolloUtils;
import com.moin.musicplayer.utils.Lists;
import com.moin.musicplayer.utils.MusicUtils;

import java.util.List;


public class ArtistAlbumAdapter extends ArrayAdapter<Album> {

    /**
     * The header view
     */
    private static final int ITEM_VIEW_TYPE_HEADER = 0;

    /**
     * * The data in the list.
     */
    private static final int ITEM_VIEW_TYPE_MUSIC = 1;

    /**
     * Number of views (ImageView, TextView, header)
     */
    private static final int VIEW_TYPE_COUNT = 3;

    /**
     * LayoutInflater
     */
    private final LayoutInflater mInflater;

    /**
     * Fake header
     */
    private final View mHeader;

    /**
     * The resource Id of the layout to inflate
     */
    private final int mLayoutId;

    /**
     * Image cache and image fetcher
     */
    private final ImageFetcher mImageFetcher;

    /**
     * Used to set the size of the data in the adapter
     */
    private List<Album> mCount = Lists.newArrayList();

    
    public ArtistAlbumAdapter(final Activity context, final int layoutId) {
        super(context, 0);
        // Used to create the custom layout
        mInflater = LayoutInflater.from(context);
        // Cache the header
        mHeader = mInflater.inflate(R.layout.faux_carousel, null);
        // Get the layout Id
        mLayoutId = layoutId;
        // Initialize the cache & image fetcher
        mImageFetcher = ApolloUtils.getImageFetcher(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        // Return a faux header at position 0
        if (position == 0) {
            return mHeader;
        }

        // Recycle MusicHolder's items
        MusicHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(mLayoutId, parent, false);
            holder = new MusicHolder(convertView);
            // Remove the background layer
            holder.mOverlay.get().setBackgroundColor(0);
            convertView.setTag(holder);
        } else {
            holder = (MusicHolder)convertView.getTag();
        }

        // Retrieve the album
        final Album album = getItem(position - 1);
        final String albumName = album.mAlbumName;

        // Set each album name (line one)
        holder.mLineOne.get().setText(albumName);
        // Set the number of songs (line two)
        holder.mLineTwo.get().setText(MusicUtils.makeLabel(getContext(),
                R.plurals.Nsongs, album.mSongNumber));
        // Set the album year (line three)
        holder.mLineThree.get().setText(album.mYear);
        // Asynchronously load the album images into the adapter
        mImageFetcher.loadAlbumImage(album.mArtistName, albumName, album.mAlbumId,
                holder.mImage.get());
        // Play the album when the artwork is touched
        playAlbum(holder.mImage.get(), position);
        return convertView;
    }

   
    @Override
    public boolean hasStableIds() {
        return true;
    }

    
    @Override
    public int getCount() {
        final int size = mCount.size();
        return size == 0 ? 0 : size + 1;
    }

    
    @Override
    public long getItemId(final int position) {
        if (position == 0) {
            return -1;
        }
        return position - 1;
    }

   
    @Override
    public int getViewTypeCount() {
        return VIEW_TYPE_COUNT;
    }

  
    @Override
    public int getItemViewType(final int position) {
        if (position == 0) {
            return ITEM_VIEW_TYPE_HEADER;
        }
        return ITEM_VIEW_TYPE_MUSIC;
    }

    
    private void playAlbum(final ImageView album, final int position) {
        album.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                final long id = getItem(position - 1).mAlbumId;
                final long[] list = MusicUtils.getSongListForAlbum(getContext(), id);
                MusicUtils.playAll(getContext(), list, 0, false);
            }
        });
    }

    /**
     * Method that unloads and clears the items in the adapter
     */
    public void unload() {
        clear();
    }

    /**
     * @param pause True to temporarily pause the disk cache, false otherwise.
     */
    public void setPauseDiskCache(final boolean pause) {
        if (mImageFetcher != null) {
            mImageFetcher.setPauseDiskCache(pause);
        }
    }

    /**
     * @param album The key used to find the cached album to remove
     */
    public void removeFromCache(final Album album) {
        if (mImageFetcher != null) {
            mImageFetcher.removeFromCache(album.mAlbumName + Config.ALBUM_ART_SUFFIX);
        }
    }

    /**
     * @param data The {@link List} used to return the count for the adapter.
     */
    public void setCount(final List<Album> data) {
        mCount = data;
    }

    /**
     * Flushes the disk cache.
     */
    public void flush() {
        mImageFetcher.flush();
    }
}
