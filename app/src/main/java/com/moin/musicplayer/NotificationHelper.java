

package com.moin.musicplayer;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.moin.musicplayer.ui.activities.HomeActivity;
import com.moin.musicplayer.utils.ApolloUtils;


@SuppressLint("NewApi")
public class NotificationHelper {

    /**
     * Notification ID
     */
    private static final int APOLLO_MUSIC_SERVICE = 1;

    /**
     * NotificationManager
     */
    private NotificationManager mNotificationManager;

    /**
     * Context
     */
    private final MusicPlaybackService mService;

    /**
     * Custom notification layout
     */
    private RemoteViews mNotificationTemplate;

    /**
     * The Notification
     */
    private Notification mNotification = null;

    /**
     * API 16+ bigContentView
     */
    private RemoteViews mExpandedView;

    /**
     * Constructor of <code>NotificationHelper</code>
     *
     * @param service The {@link Context} to use
     */
    public NotificationHelper(final MusicPlaybackService service) {
        mService = service;
        mNotificationManager = (NotificationManager)service
                .getSystemService(Context.NOTIFICATION_SERVICE);
        //
        /*int icon = R.drawable.stat_notify_music1; 
        long when = System.currentTimeMillis(); //now
        mNotification = new Notification(icon, null, when);

        Intent notificationIntent = new Intent(service, HomeActivity.class);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent intent = PendingIntent.getActivity(service, 0,
                notificationIntent, 0);

        mNotification.setLatestEventInfo(service, null, null, intent);
        mNotification.flags |= Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(0, mNotification);*/
        //
    }

    /**
     * Call this to build the {@link Notification}.
     */
    public void buildNotification(final String albumName, final String artistName,
            final String trackName, final Long albumId, final Bitmap albumArt,
            final boolean isPlaying) {

        // Default notfication layout
        mNotificationTemplate = new RemoteViews(mService.getPackageName(),
                R.layout.notification_template_base);

               
        // Set up the content view
        initCollapsedLayout(trackName, artistName, albumArt);

        // Notification Builder
        mNotification = new NotificationCompat.Builder(mService)
                .setSmallIcon(R.drawable.stat_notify_music1)
                .setContentIntent(getPendingIntent())
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setContent(mNotificationTemplate)
                .build();
        
        //
        int icon = R.drawable.default_artwork_noti;
        long when = System.currentTimeMillis(); //now
        mNotification = new Notification(icon, null, when);

        Intent notificationIntent = new Intent(mService, HomeActivity.class);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent intent = PendingIntent.getActivity(mService, 0,
                notificationIntent, 0);
        //Error here
        //mNotification.setLatestEventInfo(mService, null, null, intent);
       /* int currentapiVersion = 9;
        if (currentapiVersion < android.os.Build.VERSION_CODES.HONEYCOMB) {

            mNotification = new Notification(icon, null, when);
            mNotification.setLatestEventInfo(mService, null, null, intent); // This method is removed from the Android 6.0
            mNotification.flags |= Notification.FLAG_AUTO_CANCEL;
            //mNotification.notify(NOTIFICATION, notification);
            mNotificationManager.notify(0, mNotification);
        } *///else {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(mService);
            mNotification = builder.setContentIntent(intent)
                    .setSmallIcon(icon).setTicker(null).setWhen(when)
                    .setAutoCancel(true).setContentTitle(null)
                    .setContentText(null).build();

           // mNotification.notify(NOTIFICATION, notification);
           // mNotificationManager.notify(0, mNotification);
       // }
        ////////////////////////////////

        mNotification.flags |= Notification.FLAG_AUTO_CANCEL;
        //mNotificationManager.notify(0, mNotification);
        //
        // Control playback from the notification
        initPlaybackActions(isPlaying);
        if (ApolloUtils.hasJellyBean()) {
            // Expanded notification style
            mExpandedView = new RemoteViews(mService.getPackageName(),
                    R.layout.notification_template_expanded_base);
            mNotification.bigContentView = mExpandedView;
            // Control playback from the notification
            initExpandedPlaybackActions(isPlaying);
            // Set up the expanded content view
            initExpandedLayout(trackName, albumName, artistName, albumArt);
        }
        mService.startForeground(APOLLO_MUSIC_SERVICE, mNotification);
        
		//
        //mService = service;
       // mNotificationManager = (NotificationManager)service
               // .getSystemService(Context.NOTIFICATION_SERVICE);
        /*int icon = R.drawable.default_artwork; 
        long when = System.currentTimeMillis(); //now
        mNotification = new Notification(icon, null, when);

        Intent notificationIntent = new Intent(mService, HomeActivity.class);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent intent = PendingIntent.getActivity(mService, 0,
                notificationIntent, 0);

        mNotification.setLatestEventInfo(mService, null, null, intent);
        mNotification.flags |= Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(0, mNotification);*/
        //
     
    }

    /**
     * Changes the playback controls in and out of a paused state
     *
     * @param isPlaying True if music is playing, false otherwise
     */
    public void updatePlayState(final boolean isPlaying) {
        if (mNotification == null || mNotificationManager == null) {
            return;
        }
        if (mNotificationTemplate != null) {
            mNotificationTemplate.setImageViewResource(R.id.notification_base_play,
                    isPlaying ? R.drawable.btn_playback_pause : R.drawable.btn_playback_play);
        }

        if (ApolloUtils.hasJellyBean() && mExpandedView != null) {
            mExpandedView.setImageViewResource(R.id.notification_expanded_base_play,

                    isPlaying ? R.drawable.btn_playback_pause : R.drawable.btn_playback_play);
        }
        mNotificationManager.notify(APOLLO_MUSIC_SERVICE, mNotification);
    }

    /**
     * Open to the now playing screen
     */
    private PendingIntent getPendingIntent() {
        return PendingIntent.getActivity(mService, 0, new Intent("com.moin.musicplayer.AUDIO_PLAYER")
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK), 0);
    }

    /**
     * Lets the buttons in the remote view control playback in the expanded
     * layout
     */
    private void initExpandedPlaybackActions(boolean isPlaying) {
        // Play and pause
        mExpandedView.setOnClickPendingIntent(R.id.notification_expanded_base_play,
                retreivePlaybackActions(1));

        // Skip tracks
        mExpandedView.setOnClickPendingIntent(R.id.notification_expanded_base_next,
                retreivePlaybackActions(2));

        // Previous tracks
        mExpandedView.setOnClickPendingIntent(R.id.notification_expanded_base_previous,
                retreivePlaybackActions(3));

        // Stop and collapse the notification
        mExpandedView.setOnClickPendingIntent(R.id.notification_expanded_base_collapse,
                retreivePlaybackActions(4));

        // Update the play button image
        mExpandedView.setImageViewResource(R.id.notification_expanded_base_play,
                isPlaying ? R.drawable.btn_playback_pause : R.drawable.btn_playback_play);
    }

    /**
     * Lets the buttons in the remote view control playback in the normal layout
     */
    private void initPlaybackActions(boolean isPlaying) {
        // Play and pause
        mNotificationTemplate.setOnClickPendingIntent(R.id.notification_base_play,
                retreivePlaybackActions(1));

        // Skip tracks
        mNotificationTemplate.setOnClickPendingIntent(R.id.notification_base_next,
                retreivePlaybackActions(2));

        // Previous tracks
        mNotificationTemplate.setOnClickPendingIntent(R.id.notification_base_previous,
                retreivePlaybackActions(3));

        // Stop and collapse the notification
        mNotificationTemplate.setOnClickPendingIntent(R.id.notification_base_collapse,
                retreivePlaybackActions(4));

        // Update the play button image
        mNotificationTemplate.setImageViewResource(R.id.notification_base_play,
                isPlaying ? R.drawable.btn_playback_play : R.drawable.btn_playback_pause);
    }

    /**
     * @param which Which {@link PendingIntent} to return
     * @return A {@link PendingIntent} ready to control playback
     */
    private final PendingIntent retreivePlaybackActions(final int which) {
        Intent action;
        PendingIntent pendingIntent;
        final ComponentName serviceName = new ComponentName(mService, MusicPlaybackService.class);
        switch (which) {
            case 1:
                // Play and pause
                action = new Intent(MusicPlaybackService.TOGGLEPAUSE_ACTION);
                action.setComponent(serviceName);
                pendingIntent = PendingIntent.getService(mService, 1, action, 0);
                return pendingIntent;
            case 2:
                // Skip tracks
                action = new Intent(MusicPlaybackService.NEXT_ACTION);
                action.setComponent(serviceName);
                pendingIntent = PendingIntent.getService(mService, 2, action, 0);
                return pendingIntent;
            case 3:
                // Previous tracks
                action = new Intent(MusicPlaybackService.PREVIOUS_ACTION);
                action.setComponent(serviceName);
                pendingIntent = PendingIntent.getService(mService, 3, action, 0);
                return pendingIntent;
            case 4:
                // Stop and collapse the notification
                action = new Intent(MusicPlaybackService.STOP_ACTION);
                action.setComponent(serviceName);
                pendingIntent = PendingIntent.getService(mService, 4, action, 0);
                return pendingIntent;
            default:
                break;
        }
        return null;
    }

    /**
     * Sets the track name, artist name, and album art in the normal layout
     */
    private void initCollapsedLayout(final String trackName, final String artistName,
            final Bitmap albumArt) {
        // Track name (line one)
        mNotificationTemplate.setTextViewText(R.id.notification_base_line_one, trackName);
        // Artist name (line two)
        mNotificationTemplate.setTextViewText(R.id.notification_base_line_two, artistName);
        // Album art
        mNotificationTemplate.setImageViewBitmap(R.id.notification_base_image, albumArt);
    }

    /**
     * Sets the track name, album name, artist name, and album art in the
     * expanded layout
     */
    private void initExpandedLayout(final String trackName, final String artistName,
            final String albumName, final Bitmap albumArt) {
        // Track name (line one)
        mExpandedView.setTextViewText(R.id.notification_expanded_base_line_one, trackName);
        // Album name (line two)
        mExpandedView.setTextViewText(R.id.notification_expanded_base_line_two, albumName);
        // Artist name (line three)
        mExpandedView.setTextViewText(R.id.notification_expanded_base_line_three, artistName);
        // Album art
        mExpandedView.setImageViewBitmap(R.id.notification_expanded_base_image, albumArt);
    }

}
