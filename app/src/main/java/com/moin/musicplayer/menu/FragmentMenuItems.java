

package com.moin.musicplayer.menu;


public class FragmentMenuItems {

    /* Removes a single album from the recents pages */
    public static final int REMOVE_FROM_RECENT = 0;

    /* Used to play the selected artist, album, song, playlist, or genre */
    public static final int PLAY_SELECTION = 1;

    /* Used to add to the qeueue */
    public static final int ADD_TO_QUEUE = 2;

    /* Used to add to a playlist */
    public static final int ADD_TO_PLAYLIST = 3;

    /* Used to add to the favorites cache */
    public static final int ADD_TO_FAVORITES = 4;

    /* Used to create a new playlist */
    public static final int NEW_PLAYLIST = 5;

    /* Used to rename a playlist */
    public static final int RENAME_PLAYLIST = 6;

    /* Used to add to a current playlist */
    public static final int PLAYLIST_SELECTED = 7;

    /* Used to show more content by an artist */
    public static final int MORE_BY_ARTIST = 8;

    /* Used to delete track(s) */
    public static final int DELETE = 9;

    /* Used to fetch an artist image */
    public static final int FETCH_ARTIST_IMAGE = 10;

    /* Used to fetch album art */
    public static final int FETCH_ALBUM_ART = 11;

    /* Used to set a track as a ringtone */
    public static final int USE_AS_RINGTONE = 12;

    /* Used to remove a track from the favorites cache */
    public static final int REMOVE_FROM_FAVORITES = 13;

    /* Used to remove a track from a playlist */
    public static final int REMOVE_FROM_PLAYLIST = 14;

    /* Used to remove a track from the queue */
    public static final int REMOVE_FROM_QUEUE = 15;

    /* Used to queue a track to be played next */
    public static final int PLAY_NEXT = 16;

}
