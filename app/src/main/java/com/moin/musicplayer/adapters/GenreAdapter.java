
package com.moin.musicplayer.adapters;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.moin.musicplayer.R;
import com.moin.musicplayer.model.Genre;
import com.moin.musicplayer.ui.MusicHolder;
import com.moin.musicplayer.ui.MusicHolder.DataHolder;
import com.moin.musicplayer.ui.fragments.GenreFragment;

public class GenreAdapter extends ArrayAdapter<Genre> {

    
    private static final int VIEW_TYPE_COUNT = 1;

    
    private final int mLayoutId;

    private DataHolder[] mData;

    public GenreAdapter(final Context context, final int layoutId) {
        super(context, 0);
        // Get the layout Id
        mLayoutId = layoutId;
    }

   
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        // Recycle ViewHolder's items
        MusicHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(mLayoutId, parent, false);
            holder = new MusicHolder(convertView);
            // Hide the second and third lines of text
            holder.mLineTwo.get().setVisibility(View.GONE);
            holder.mLineThree.get().setVisibility(View.GONE);
            // Make line one slightly larger
            holder.mLineOne.get().setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    getContext().getResources().getDimension(R.dimen.text_size_large));
            convertView.setTag(holder);
        } else {
            holder = (MusicHolder)convertView.getTag();
        }

        // Retrieve the data holder
        final DataHolder dataHolder = mData[position];

        // Set each genre name (line one)
        holder.mLineOne.get().setText(dataHolder.mLineOne);
        return convertView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasStableIds() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getViewTypeCount() {
        return VIEW_TYPE_COUNT;
    }

    
    public void buildCache() {
        mData = new DataHolder[getCount()];
        for (int i = 0; i < getCount(); i++) {
            // Build the artist
            final Genre genre = getItem(i);

            // Build the data holder
            mData[i] = new DataHolder();
            // Genre Id
            mData[i].mItemId = genre.mGenreId;
            // Genre names (line one)
            mData[i].mLineOne = genre.mGenreName;
        }
    }

   
    public void unload() {
        clear();
        mData = null;
    }

}
