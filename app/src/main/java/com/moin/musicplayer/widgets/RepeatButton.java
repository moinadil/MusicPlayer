

package com.moin.musicplayer.widgets;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ImageButton;

import com.moin.musicplayer.R;
import com.moin.musicplayer.MusicPlaybackService;
import com.moin.musicplayer.utils.ApolloUtils;
import com.moin.musicplayer.utils.MusicUtils;
import com.moin.musicplayer.utils.ThemeUtils;
import com.moin.musicplayer.widgets.theme.HoloSelector;


public class RepeatButton extends ImageButton implements OnClickListener, OnLongClickListener {

    /**
     * Repeat one theme resource
     */
    private static final String REPEAT_ALL = "btn_playback_repeat_all";

    /**
     * Repeat one theme resource
     */
    private static final String REPEAT_CURRENT = "btn_playback_repeat_one";

    /**
     * Repeat one theme resource
     */
    private static final String REPEAT_NONE = "btn_playback_repeat";

    /**
     * The resources to use.
     */
    private final ThemeUtils mResources;

    /**
     * @param context The {@link Context} to use
     * @param attrs The attributes of the XML tag that is inflating the view.
     */
    @SuppressWarnings("deprecation")
    public RepeatButton(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        // Initialze the theme resources
        mResources = new ThemeUtils(context);
        // Set the selector
        setBackgroundDrawable(new HoloSelector(context));
        // Control playback (cycle repeat modes)
        setOnClickListener(this);
        // Show the cheat sheet
        setOnLongClickListener(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onClick(final View v) {
        MusicUtils.cycleRepeat();
        updateRepeatState();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onLongClick(final View view) {
        if (TextUtils.isEmpty(view.getContentDescription())) {
            return false;
        } else {
            ApolloUtils.showCheatSheet(view);
            return true;
        }
    }

    /**
     * Sets the correct drawable for the repeat state.
     */
    public void updateRepeatState() {
        switch (MusicUtils.getRepeatMode()) {
            case MusicPlaybackService.REPEAT_ALL:
                setContentDescription(getResources().getString(R.string.accessibility_repeat_all));
                setImageDrawable(mResources.getDrawable(REPEAT_ALL));
                break;
            case MusicPlaybackService.REPEAT_CURRENT:
                setContentDescription(getResources().getString(R.string.accessibility_repeat_one));
                setImageDrawable(mResources.getDrawable(REPEAT_CURRENT));
                break;
            case MusicPlaybackService.REPEAT_NONE:
                setContentDescription(getResources().getString(R.string.accessibility_repeat));
                setImageDrawable(mResources.getDrawable(REPEAT_NONE));
                break;
            default:
                break;
        }
    }
}
