

package com.moin.musicplayer.widgets;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ImageButton;

import com.moin.musicplayer.R;
import com.moin.musicplayer.MusicPlaybackService;
import com.moin.musicplayer.utils.ApolloUtils;
import com.moin.musicplayer.utils.MusicUtils;
import com.moin.musicplayer.utils.ThemeUtils;
import com.moin.musicplayer.widgets.theme.HoloSelector;


public class ShuffleButton extends ImageButton implements OnClickListener, OnLongClickListener {

    /**
     * Shuffle theme resource
     */
    private static final String SHUFFLE = "btn_playback_shuffle";

    /**
     * Shuffle all theme resource
     */
    private static final String SHUFFLE_ALL = "btn_playback_shuffle_all";

    /**
     * The resources to use.
     */
    private final ThemeUtils mResources;

    /**
     * @param context The {@link Context} to use
     * @param attrs The attributes of the XML tag that is inflating the view.
     */
    @SuppressWarnings("deprecation")
    public ShuffleButton(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        // Initialze the theme resources
        mResources = new ThemeUtils(context);
        // Theme the selector
        setBackgroundDrawable(new HoloSelector(context));
        // Control playback (cycle shuffle)
        setOnClickListener(this);
        // Show the cheat sheet
        setOnLongClickListener(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onClick(final View v) {
        MusicUtils.cycleShuffle();
        updateShuffleState();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onLongClick(final View view) {
        if (TextUtils.isEmpty(view.getContentDescription())) {
            return false;
        } else {
            ApolloUtils.showCheatSheet(view);
            return true;
        }
    }

    /**
     * Sets the correct drawable for the shuffle state.
     */
    public void updateShuffleState() {
        switch (MusicUtils.getShuffleMode()) {
            case MusicPlaybackService.SHUFFLE_NORMAL:
                setContentDescription(getResources().getString(R.string.accessibility_shuffle_all));
                setImageDrawable(mResources.getDrawable(SHUFFLE_ALL));
                break;
            case MusicPlaybackService.SHUFFLE_AUTO:
                setContentDescription(getResources().getString(R.string.accessibility_shuffle_all));
                setImageDrawable(mResources.getDrawable(SHUFFLE_ALL));
                break;
            case MusicPlaybackService.SHUFFLE_NONE:
                setContentDescription(getResources().getString(R.string.accessibility_shuffle));
                setImageDrawable(mResources.getDrawable(SHUFFLE));
                break;
            default:
                break;
        }
    }

}
