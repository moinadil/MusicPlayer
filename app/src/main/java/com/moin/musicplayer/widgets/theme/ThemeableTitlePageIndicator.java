

package com.moin.musicplayer.widgets.theme;

import android.content.Context;
import android.util.AttributeSet;

import com.moin.musicplayer.utils.ThemeUtils;
import com.viewpagerindicator.TitlePageIndicator;


public class ThemeableTitlePageIndicator extends TitlePageIndicator {

    /**
     * Resource name used to theme the background
     */
    private static final String BACKGROUND = "tpi_background";

    /**
     * Resource name used to theme the selected text color
     */
    private static final String SELECTED_TEXT = "tpi_selected_text_color";

    /**
     * Resource name used to theme the unselected text color
     */
    private static final String TEXT = "tpi_unselected_text_color";

    /**
     * Resource name used to theme the footer color
     */
    private static final String FOOTER = "tpi_footer_color";

    /**
     * @param context The {@link Context} to use
     * @param attrs The attributes of the XML tag that is inflating the view.
     */
    @SuppressWarnings("deprecation")
    public ThemeableTitlePageIndicator(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        // Initialze the theme resources
        final ThemeUtils resources = new ThemeUtils(context);
        // Theme the background
        setBackgroundDrawable(resources.getDrawable(BACKGROUND));
        // Theme the selected text color
        setSelectedColor(resources.getColor(SELECTED_TEXT));
        // Theme the unselected text color
        setTextColor(resources.getColor(TEXT));
        // Theme the footer
        setFooterColor(resources.getColor(FOOTER));
    }
}
