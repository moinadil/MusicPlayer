

package com.moin.musicplayer.lastfm;

import java.util.Collection;
import java.util.Iterator;


public class PaginatedResult<T> implements Iterable<T> {

    private final int page;

    private final int totalPages;

    public final Collection<T> pageResults;

    /**
     * @param page
     * @param totalPages
     * @param pageResults
     */
    PaginatedResult(final int page, final int totalPages, final Collection<T> pageResults) {
        this.page = page;
        this.totalPages = totalPages;
        this.pageResults = pageResults;
    }

    /**
     * Returns the page number of this result.
     * 
     * @return page number
     */
    public int getPage() {
        return page;
    }

    /**
     * Returns the total number of pages available.
     * 
     * @return total pages
     */
    public int getTotalPages() {
        return totalPages;
    }

   
    public boolean isEmpty() {
        return pageResults == null || pageResults.isEmpty();
    }

    @Override
    public Iterator<T> iterator() {
        return pageResults.iterator();
    }
}
