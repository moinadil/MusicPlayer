

package com.moin.musicplayer.lastfm;


interface ItemFactory<T> {

    
    public T createItemFromElement(DomElement element);

}
