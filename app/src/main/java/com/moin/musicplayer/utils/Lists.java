

package com.moin.musicplayer.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;


public final class Lists {

    /** This class is never instantiated */
    public Lists() {
    }

    
    public static final <E> ArrayList<E> newArrayList() {
        return new ArrayList<E>();
    }

    
    public static final <E> LinkedList<E> newLinkedList() {
        return new LinkedList<E>();
    }

}
